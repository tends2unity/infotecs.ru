package runner;

import XmlToJson.JDOMxmlReader;
import XmlToJson.TestPlan;
import com.google.gson.Gson;
import org.xml.sax.SAXException;

import javax.xml.parsers.ParserConfigurationException;
import java.io.*;
import java.util.Date;
import java.util.List;

public class MainClass {
    static final String FILE_XML = "file.xml";
    static final String JSON_FILE = "file.json";

    public static void main(String[] args) {
        if(args == null || args.length == 0){
            System.out.println("Input args 1 to 3");
        }else {


        switch (args[0]) {
            case "1": {
                File file = new File("tmpFile");
                int bufSize = 1024*1024;
                byte[] blob = new byte[bufSize];

                try (FileOutputStream fos=new FileOutputStream(file))
                {
                    int n=0;
                    for(int i = 0; i < 1024; ++i){
                        fos.write(blob);
                        n++;
                        if (n == 8){
                            System.out.print("\r" + i*bufSize/1024 + " KB writed");
                            n = 0;
                        }
                    }
                    fos.flush();
                    fos.close();
                } catch (IOException e) {
                    System.err.println("Ошибка ввода вывода: ");
                    e.printStackTrace();
                }finally {
                    System.out.println("\nfile.length = " + file.length());
                    file.deleteOnExit();
                }


                break;
            }
            case "2": {
                ProcessBuilder procBuilder = new ProcessBuilder("ping", "infotecs.ru");
                procBuilder.redirectErrorStream(true);

                Process process = null;
                try {
                    process = procBuilder.start();


                    InputStream stdout = process.getInputStream();
                    InputStreamReader isrStdout = new InputStreamReader(stdout);
                    BufferedReader brStdout = new BufferedReader(isrStdout);


                    long startTime = System.currentTimeMillis();
                    long elapsedTime = 0L;
                    String line;

                    while (elapsedTime < 10 * 1000) {


                        if ((line = brStdout.readLine()) != null) {
                            System.out.println(line);
                        }

                        elapsedTime = (new Date()).getTime() - startTime;
                    }
                } catch (IOException e) {
                    System.err.println("Ошибка ввода вывода: ");
                    e.printStackTrace();
                }

                break;
            }
            case "3": {
                List<TestPlan> testPlans = null;
                try {
                    testPlans = JDOMxmlReader.JDOMxmlReader(FILE_XML);

                    Gson gson = new Gson();
                    File gsonFile = new File(JSON_FILE);
                    BufferedWriter bw = new BufferedWriter(new OutputStreamWriter(new FileOutputStream(gsonFile)));
                    bw.write(gson.toJson(testPlans));
                    bw.flush();
                    bw.close();
                } catch (ParserConfigurationException e) {
                    System.err.println("Ошибка конфигурации JDOMreader'a: ");
                    e.printStackTrace();
                } catch (IOException e) {
                    System.err.println("Ошибка ввода вывода: ");
                    e.printStackTrace();
                } catch (SAXException e) {
                    System.err.println("Ошибка разбора XML: ");
                    e.printStackTrace();
                }
                break;
            }
        }
        }
    }
}
