package XmlToJson;

import org.jdom2.Element;
import org.jdom2.input.DOMBuilder;
import org.w3c.dom.Document;
import org.xml.sax.SAXException;

import javax.xml.parsers.DocumentBuilder;
import javax.xml.parsers.DocumentBuilderFactory;
import javax.xml.parsers.ParserConfigurationException;
import java.io.File;
import java.io.IOException;
import java.util.ArrayList;
import java.util.List;

public class JDOMxmlReader {
//    public static void main(String[] args) throws ParserConfigurationException, IOException, SAXException {
    public static List<TestPlan> JDOMxmlReader(String fileName) throws ParserConfigurationException, IOException, SAXException {

        org.jdom2.Document jdomDocument = createJDOMusingDOMParser(fileName);
        Element root = jdomDocument.getRootElement();
        List<Element> testPlanListElements = root.getChildren("testPlan");
        List<TestPlan> testPlans = new ArrayList<>();
        for (Element plan : testPlanListElements) {
            TestPlan testPlan = new TestPlan();
            testPlan.setPath(plan.getAttributeValue("path"));
            if (plan.getAttributeValue("timeToWait") != null){

                testPlan.setTimeToWait(Integer.parseInt(plan.getAttributeValue("timeToWait")));
            }

            OutLog outLog = new OutLog(plan.getChildText("outLog"));
            testPlan.setOutLog(outLog);

            testPlans.add(testPlan);
        }
//        System.out.println(root.getName());
//
//        for (TestPlan testPlan : testPlans) {
//            System.out.println(testPlan.toString());
//        }

        return testPlans;
    }

    private static org.jdom2.Document createJDOMusingDOMParser(String fileName) throws ParserConfigurationException, IOException, SAXException {
        //создаем DOM Document
        DocumentBuilderFactory dbFactory = DocumentBuilderFactory.newInstance();
        DocumentBuilder documentBuilder;
        documentBuilder = dbFactory.newDocumentBuilder();
        Document doc = documentBuilder.parse(new File(fileName));
        DOMBuilder domBuilder = new DOMBuilder();

        return domBuilder.build(doc);
    }
}
