package XmlToJson;


public class TestPlan {
    private String path;
    private Integer timeToWait;
    private OutLog outLog;


    public String getPath() {
        return path;
    }

    public void setPath(String path) {
        this.path = path;
    }

    public Integer getTimeToWait() {
        return timeToWait;
    }

    public void setTimeToWait(Integer timeToWait) {
        this.timeToWait = timeToWait;
    }

    public OutLog getOutLog() {
        return outLog;
    }

    public void setOutLog(OutLog outLog) {
        this.outLog = outLog;
    }


    @Override
    public String toString() {
        return "TestPlan{" +
                "path='" + path + '\'' +
                ", timeToWait=" + timeToWait +
                ", outLog='" + outLog + '\'' +
                '}';
    }
}
