package XmlToJson;

public class OutLog {
    private String value;

    public OutLog(String value) {
        this.value = value;
    }

    public String getValue() {
        return value;
    }

    public void setValue(String value) {
        this.value = value;
    }
}
